#Entrega do desafio técnico Full Cycle Develope

<p align="center">Desenvolvido com <img src="stk.png"></p>

# Pré-requisitos:
- Ruby.
- Gem
- Google chrome Versão 87.0.4280.141 + e o seu <a href="https://chromedriver.chromium.org/downloads"> driver</a> /  
#EXTRAIA O ARQUIVO DENTRO DA PASTA USR/LOCAL/BIN PARA MAC E LINUX. PARA WINDOWS DENTRO DA PASTA C:/WINDOWS.


 # Dependências:
 - cucumber.
 - rspec.
 - capybara.
 - selenium-webdriver
 
 # Passos para rodar os teste:
 - Atender aos pré-requisitos.
 - Clonar esse repositório .
 - acessar o diretório raiz do projeto .
 - executar o comando: bundle install.
 - executar o comando: cucumber ----publish

 # Relatório:
 - Ao final o cucumber gera o relatório dos testes, observe no console o endereço para visualização.

 #Grato - Renan
