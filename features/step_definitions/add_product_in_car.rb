 @pontos = 0
 
 Dado('que acesso o clube livelo') do
    visit ('https://www.livelo.com.br/')
    
  end
 Então('eu me certifico de que estou no site correto') do
    expect(page).to have_current_path('https://www.livelo.com.br/')
 end

Dado('que acesso a {string} do produto') do |url|
  visit(url)
  
end

Quando('verifico de o {string} esta correto') do |produto|
  expect(page.find('.h2').text).to eq(produto)
end

Quando('seleciono a voltagem') do
  select '110v', from: 'CC-prodDetails-sku-type_other_v_voltage'
end

Quando('adiciono ao carinho') do
  find('#cc-prodDetails-addToCart').click
  
end

Então('o {string}  é adicionado ao carrinho') do |produto|
  text = page.find('#cc-cart-total').text
  text = text.gsub " Pontos", "" 
  num =  text.to_i
  expect(true).to eq(num>0)
 
end
Dado('excluo o item do carrinho') do
   click_link('Remover')
  sleep(5)
end

Então('o item é excluido') do
 text = find('.h3').text
 expect('Ainda não existem itens no seu carrinho').to eq(text)
end

