#language: pt

Funcionalidade: adicionar um produto ao carinho.

- Como usuario do clube livelo 
- Quero adicionar um produto ao carrinho

Cenario: Acessar o clube livelo
Dado que acesso o clube livelo
Então eu me certifico de que estou no site correto

Esquema do Cenario: adicionar produto ao carrinho.
Dado que acesso a <url> do produto
Quando verifico de o <produto> esta correto
E seleciono a voltagem 
E adiciono ao carinho 
Então o <produto>  é adicionado ao carrinho 

Exemplos:   
|                   produto                                |                                url                                        |         
|           "Sanduicheira Grill Ultra S13 - Preto"         |      "sanduicheira-grill-ultra-s13-preto/produto/LVL187788"               |
|"Escova Rotativa Philco Beauty Shine Single PEC03 - Roxo" |"escova-rotativa-philco-beauty-shine-single-pec03-roxo/produto/LVL13686157"|
 
Cenario: remover produto do carrinho.
Dado que acesso a "https://www.livelo.com.br/jogo-de-chaves-de-fenda-tramontina-6-pe%C3%A7as/produto/LVL182009" do produto
E adiciono ao carinho 
E excluo o item do carrinho
Então o item é excluido